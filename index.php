<?php
	include('head.php');
	if(!isset($_GET["page"])){
		$title = array("Strona Główna", "Ostatnio Dodane");
		$art = $db->Execute("SELECT ID, Title FROM Article ORDER BY PublishDate DESC Limit 0,10");
		$il = $art->RecordCount();
		$a = array();
		while(!$art->EOF){
			array_push($a, array($art->Fields('ID'), $art->Fields('Title')));
			$art->MoveNext();
		}
		$smarty->assign("title", $title);
		$smarty->assign("wellcome", $site->Fields('MainWellcome'));
		$smarty->assign("last", $a);
		$smarty->assign("lastil", $il);
		$smarty->display('index.tpl');
	}
	if(isset($_GET["page"]) && $_GET["page"] == "contact"){
		$title = "Kontakt";
		$smarty->assign("title", $title);
		$smarty->display('kontakt.tpl');
	}
?>