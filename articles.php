<?php
	include('head.php');
	if(isset($_GET["cat"]) && !isset($_GET["art"])){
		$cat = $db->Execute("SELECT * FROM Categories WHERE ID=".$_GET["cat"]);
		$title = $cat->Fields('CategoryName');
		$articles = $db->Execute("SELECT * FROM Article WHERE CategoryID=".$_GET["cat"]." ORDER BY PublishDate DESC");
		$il = $articles->RecordCount();
		$artic = array();
		while(!$articles->EOF){
			array_push($artic, array($articles->Fields('ID'), $articles->Fields('Title')));
			$articles->MoveNext();
		}
		$smarty->assign("title", $title);
		$smarty->assign("artil", $il);
		$smarty->assign("art", $artic);
		$smarty->display('category.tpl');
	}
	else if(isset($_GET["art"])){
		$article = $db->Execute("SELECT * FROM Article WHERE ID=".$_GET["art"]);
		$il = $article->RecordCount();
		$smarty->assign("il", $il);
		if(!$article->EOF){
			$smarty->assign("Article", array("Title" => $article->Fields('Title'),
											 "FirstTitle" => $article->Fields('FirstTitle'),
											 "SecondTitle" => $article->Fields('SecondTitle'),
											 "ThirdTitle" => $article->Fields('ThirdTitle'),
											 "Text" => $article->Fields('Text'),
											 "SecondText" => $article->Fields('SecondText'),
											 "ThirdText" => $article->Fields('ThirdText'),
											 "Publisher" => $article->Fields('Publisher'),
											 "PublishDate" => $article->Fields('PublishDate')));
		}
		$smarty->display('article.tpl');
	}
?>