<?php
	include('../PHP_LIBS/Smarty/libs/Smarty.class.php');
	include('config.php');
	
	$site = $db->Execute("SELECT * FROM Properties WHERE ID=1");
	$page = $db->Execute("SELECT * FROM Pages");
	$p = array();
	$c = array();
	while(!$page->EOF){
		array_push($p, $page->Fields('PageName'));
		$tmp = array();
		$cat = $db->Execute("SELECT * FROM Categories WHERE PageID=".$page->Fields('ID'));
		while(!$cat->EOF){
			array_push($tmp, array($cat->Fields('ID'), $cat->Fields('CategoryName')));
			$cat->MoveNext();
		}
		array_push($c, $tmp);
		$page->MoveNext();
	}
	$smarty = new Smarty;
	$smarty->caching = false;
	
	$smarty->assign("site", $site->Fields('SiteName'));
	$smarty->assign("page", $p);
	$smarty->assign("cat", $c);
	
	$smarty->display('head.tpl');
?>