<? 
	include('head.php');
	if(!isset($_GET["page"])){
?>
			<div id="MAIN">
            	<table border="0" cellpadding="0" cellspacing="0" width="486">
                	<tr>
                    	<td class="main-up-left"></td>
                        <td class="main-up-center" colspan="3"><font class="headers-font">Strona Główna</font></td>
                        <td class="main-up-right"></td>
                    </tr>
                    <tr>
                    	<td class="main-center-left"></td>
                        <td class="main-center" colspan="3"><font class="font"><br><?php echo $site['MainWellcome']; ?><br></font></td>
                        <td class="main-center-right"></td>
                    </tr>
                    <tr>
                    	<td class="main-down-left"></td>
                        <td class="main-down-left-center"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td height="15"></td></tr><tr><td height="40" align="center" valign="middle"><font class="sign-font"><?php echo $site['AdminName']; ?></font></td></tr></table></td>
                        <td class="main-down-center"></td>
                        <td class="main-down-right-center"><font class="date-font">Opublikowano dnia: <?php echo $site['OpenDate']; ?><br>O godzinie: <?php echo $site['OpenHour']; ?></font></td>
                        <td class="main-down-right"></td>
                    </tr>
                </table><br>
                <table border="0" cellpadding="0" cellspacing="0" width="486">
                	<tr>
                    	<td class="main-up-left"></td>
                        <td class="main-up-center" colspan="3"><font class="headers-font">Ostatnio Dodane</font></td>
                        <td class="main-up-right"></td>
                    </tr>
                    <tr>
                    	<td class="main-center-left"></td>
                        <td class="main-center" colspan="3"><br><font class="font">Lista ostatnio dodanych artykułów.</font><ul>
<?
		$q = mysql_query("SELECT Articles.PageID, Articles.CategoryID, Articles.ArticleID, Article.Title FROM Articles INNER JOIN Article ON Articles.ArticleID=Article.ArticleID ORDER BY PublishDate DESC");
		$r = mysql_fetch_array($q);
		for($i=0;$i<10;$i++){
			if($r != NULL){
				echo '<li><a class="font" href="articles.php?page='.$r['PageID'].'&amp;category='.$r['CategoryID'].'&amp;article='.$r['ArticleID'].'">'.$r['Title'].'</a></li>';
			}
			$r = mysql_fetch_array($q);
		}
?>
                        </ul></td>
                        <td class="main-center-right"></td>
                    </tr>
                    <tr>
                    	<td class="main-down-left"></td>
                        <td class="main-down-left-center"></td>
                        <td class="main-down-center"></td>
                        <td class="main-down-right-center"></td>
                        <td class="main-down-right"></td>
                    </tr>
                </table>
           	</div>
<? 
	}
	else if(isset($_GET["page"]) && $_GET["page"] == "Contact"){
?>
			<div id="MAIN">
            	<table border="0" cellpadding="0" cellspacing="0" width="486">
                	<tr>
                    	<td class="main-up-left"></td>
                        <td class="main-up-center" colspan="3"><font class="headers-font">KONTAKT</font></td>
                        <td class="main-up-right"></td>
                    </tr>
                    <tr>
                    	<td class="main-center-left"></td>
                        <td class="main-center" colspan="3">
<?
		if(!isset($_GET["step"])){
?>
							<form action="index.php?page=Contact&amp;step=send" method="post">
                        		<table width="100%" border="0">
                            		<tr>
                                		<td><font class="font">Email:</font></td>
                                        <td><input type="text" name="email" /></td>
                                	</tr>
                                    <tr>
                                    	<td><font class="font">Temat:</font></td>
                                        <td><input type="text" name="temat" /></td>
                                    </tr>
                                    <tr>
                                    	<td><font class="font">Treść:</font></td>
                                        <td><textarea name="tresc" cols="50" rows="10"></textarea></td>
                                    </tr>
                                    <tr>
                                    	<td colspan="2"><input type="submit" value="Wyślij" /><input type="reset" value="Wyczyść" /></td>
                                    </tr>
                            	</table>
                            </form>
<?
		}
		else if(isset($_GET["step"]) && $_GET["step"] == "send"){
			$emailto = "michau.swiatek@gmail.com";
			$email = $_POST["email"];
			$subject = $_POST["temat"];
			$message = $_POST["tresc"];
			$headers = "Form: ".$email."\r\nContent-type: text/html; charset=utf-8";
			if(mail($emailto, $subject, $message, $headers)){
?>
							<br><font class="font">Mail został Wysłany!</font>
<?
			}
			else{
?>
							<br><font class="font">BŁĄD!!<br />Nie można wysłać maila.</font>
<?
			}
		}
?>
                        </td>
                        <td class="main-center-right"></td>
                    </tr>
                    <tr>
                    	<td class="main-down-left"></td>
                        <td class="main-down-left-center"></td>
                        <td class="main-down-center"></td>
                        <td class="main-down-right-center"></td>
                        <td class="main-down-right"></td>
                    </tr>
                </table>
           	</div>
<?
	}
	else{
?>
			<div id="MAIN">
            	<table border="0" cellpadding="0" cellspacing="0" width="486">
                	<tr>
                    	<td class="main-up-left"></td>
                        <td class="main-up-center" colspan="3"><font class="headers-font">BŁĄD</font></td>
                        <td class="main-up-right"></td>
                    </tr>
                    <tr>
                    	<td class="main-center-left"></td>
                        <td class="main-center" colspan="3"><font class="font"><br>Taka strona nie istnieje!</font></td>
                        <td class="main-center-right"></td>
                    </tr>
                    <tr>
                    	<td class="main-down-left"></td>
                        <td class="main-down-left-center"></td>
                        <td class="main-down-center"></td>
                        <td class="main-down-right-center"></td>
                        <td class="main-down-right"></td>
                    </tr>
                </table>
           	</div>
<?
	}
	include('foot.php');
?>
