

--
-- Struktura tabeli dla tabeli `Article`
--

CREATE TABLE IF NOT EXISTS `Article` (
  `ArticleID` int(11) NOT NULL AUTO_INCREMENT,
  `Autor` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `Title` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `PublishDate` date NOT NULL,
  `PublishHour` time NOT NULL,
  `FirstTitle` varchar(150) COLLATE utf8_polish_ci DEFAULT NULL,
  `FirstText` text COLLATE utf8_polish_ci NOT NULL,
  `SecondTitle` varchar(150) COLLATE utf8_polish_ci DEFAULT NULL,
  `SecondText` text COLLATE utf8_polish_ci,
  `ThirdTitle` varchar(150) COLLATE utf8_polish_ci DEFAULT NULL,
  `ThirdText` text COLLATE utf8_polish_ci,
  PRIMARY KEY (`ArticleID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=3 ;

--
-- Zrzut danych tabeli `Article`
--

INSERT INTO `Article` (`ArticleID`, `Autor`, `Title`, `PublishDate`, `PublishHour`, `FirstTitle`, `FirstText`, `SecondTitle`, `SecondText`, `ThirdTitle`, `ThirdText`) VALUES
(1, 'Manveru', 'Siemak', '2013-02-05', '15:28:00', NULL, 'asgbrtregqer gqeff adsfahsdhj viajgb sfiu bdsv a hlifu bshvcal usdvcb alshjvczusd hvgfb awilvf usdv casuvfas<br>sadfas fchuasvd ckuawy veduasdvck uawvefkuasck uaesv  ufyasvdf fcvaskdufy LWEY FVAUSDHCVA KUWETFV UASDFVCHSDVKF UAGSVEU ufs aufyvg asjdfaw<br>adfasdvad gawefasdf', NULL, NULL, NULL, NULL),
(2, 'Manveru', 'Hello', '2013-02-05', '15:28:00', 'p1', 'asgbrtregqer gqeff adsfahsdhj viajgb sfiu bdsv a hlifu bshvcal usdvcb alshjvczusd hvgfb awilvf usdv casuvfas<br>sadfas fchuasvd ckuawy veduasdvck uawvefkuasck uaesv  ufyasvdf fcvaskdufy LWEY FVAUSDHCVA KUWETFV UASDFVCHSDVKF UAGSVEU ufs aufyvg asjdfaw<br>adfasdvad gawefasdf', 'p2', 'asgbrtregqer gqeff adsfahsdhj viajgb sfiu bdsv a hlifu bshvcal usdvcb alshjvczusd hvgfb awilvf usdv casuvfas<br>sadfas fchuasvd ckuawy veduasdvck uawvefkuasck uaesv  ufyasvdf fcvaskdufy LWEY FVAUSDHCVA KUWETFV UASDFVCHSDVKF UAGSVEU ufs aufyvg asjdfaw<br>adfasdvad gawefasdf', 'p3', 'asgbrtregqer gqeff adsfahsdhj viajgb sfiu bdsv a hlifu bshvcal usdvcb alshjvczusd hvgfb awilvf usdv casuvfas<br>sadfas fchuasvd ckuawy veduasdvck uawvefkuasck uaesv  ufyasvdf fcvaskdufy LWEY FVAUSDHCVA KUWETFV UASDFVCHSDVKF UAGSVEU ufs aufyvg asjdfaw<br>adfasdvad gawefasdf');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Articles`
--

CREATE TABLE IF NOT EXISTS `Articles` (
  `PageID` int(11) NOT NULL,
  `CategoryID` int(11) NOT NULL,
  `ArticleID` int(11) NOT NULL,
  PRIMARY KEY (`PageID`,`CategoryID`,`ArticleID`),
  KEY `FK_Article` (`ArticleID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `Articles`
--

INSERT INTO `Articles` (`PageID`, `CategoryID`, `ArticleID`) VALUES
(2, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Categories`
--

CREATE TABLE IF NOT EXISTS `Categories` (
  `PageID` int(11) NOT NULL,
  `CategoryID` int(11) NOT NULL AUTO_INCREMENT,
  `CategoryName` varchar(150) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`PageID`,`CategoryID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1 ;

--
-- Zrzut danych tabeli `Categories`
--

INSERT INTO `Categories` (`PageID`, `CategoryID`, `CategoryName`) VALUES
(1, 1, 'Strona Główna'),
(1, 2, 'Froum'),
(1, 3, 'Gra'),
(1, 4, 'Kontakt'),
(2, 1, 'Geografia'),
(2, 2, 'Rasy'),
(3, 1, 'Podstawowe Zasady'),
(3, 2, 'Pomoce dla MG'),
(4, 1, 'Książki'),
(4, 2, 'Gry'),
(4, 3, 'Twórczość Czytelników');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Pages`
--

CREATE TABLE IF NOT EXISTS `Pages` (
  `PageID` int(11) NOT NULL AUTO_INCREMENT,
  `PageName` varchar(150) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`PageID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=5 ;

--
-- Zrzut danych tabeli `Pages`
--

INSERT INTO `Pages` (`PageID`, `PageName`) VALUES
(1, 'Nawigacja'),
(2, 'Świat'),
(3, 'System RPG'),
(4, 'Źródła Wiedzy');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Properties`
--

CREATE TABLE IF NOT EXISTS `Properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `SiteName` varchar(150) COLLATE utf8_polish_ci NOT NULL,
  `AdminName` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `MainWellcome` longtext COLLATE utf8_polish_ci NOT NULL,
  `OpenDate` date NOT NULL,
  `OpenHour` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=2 ;

--
-- Zrzut danych tabeli `Properties`
--

INSERT INTO `Properties` (`id`, `SiteName`, `AdminName`, `MainWellcome`, `OpenDate`, `OpenHour`) VALUES
(1, 'Compedium of Warhammer', 'Manveru', '<center>Witam na stronie Compedium of Warhammer.<br>\r\nMam zaszczyt ogłosić, iż z dniem 5-02-2013 strona ruszyła.</center><br>\r\nNie ważne czy trafiłeś na nią drogi czytelniku przypadkiem, cz też z pełną świadomością. Dla tych, którzy jeszcze nie wiedzą oraz mają ochotę czytaæ ten krótki wstęp, powiem że strona traktuje o uniwersum Warhammera.<br>\r\nJeśli jeszcze nie wiesz czym jest Warhammer, to dobrze trafiłeś, masz bowiem świetną okazje by zgłębić wiedzę na ten temat.  uwierz mi nie zajmie ci wiele czasu przeczytanie kilku artykułów, a możliwe, że te artykuły zainteresują cię i zachęcą do dalszego poznawania świata.<br>\r\nOczywiście na stronie nie znajdziesz wszystkich informacji, jednak jest ona na bieżąco aktualizowana. Co rusz są dodawane nowe artykuły. Jeżeli uważasz że wiesz coś na temat Warhammer a czego nie ma na stronie a chciałbyś się tą wiedzą podzielić to masz doskonałą okazję. Wystarczy się ze mną skontaktować, lub napisać na forum odpowiedni post.<br>\r\nSkoro mowa o forum, to już teraz zapraszam do rejestrowania się i dzielenia swoimi opiniami na temat uniwersum. Już niedługo planowane jest także uruchomienie beta-testów gry przeglądarkowej opartej na systemie RPG.<br><br>\r\nA teraz życzę przyjemnej lektury:<br>Administrator Serwisu.', '2013-02-04', '12:08:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
