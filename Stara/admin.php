<?
	include('config.php');
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Panel Administracyjny!</title>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <style>
			.all {
				width: 95%;
				margin-left: 10px;
			}
			.menu {
				width: 300px;
				border-right: 1px solid black;
			}
			.main {
				width: *;
				text-align: center;
			}
			.kat {
				width: 400px;
				border: 1px solid black;
			}
			.kat_nag {
				width: 400px;
				border: 1px solid black;
				color: white;
				font: 12px bold Verdana;
				background-color: #666666;
			}
			.kat_row {
				width: 400px;
				border: 1px solid black;
				font: 10px Verdana;
			}
			.kat_td {
				width: 200px;
				border: 1px solid black;
			}
			.art {
				width: 700px;
				border: 1px solid black;
			}
			.art_nag {
				width: 700px;
				border: 1px solid black;
				color: white;
				font: 12px bold Verdana;
				background-color: #666666;
			}
			.art_row {
				width: 700px;
				border: 1px solid black;
				font: 10px Verdana;
			}
			.art_td {
				border: 1px solid black;
			}
		</style>
        <script>
		$(document).ready(function(){
  			$("#editor #gird_edit_kat").bind({
			click:function(){
				var tex = $(this).text();
				var textarea = $('<input type="text"style="width:100%; height: 20px;" id="gird_edit_kat" value="'+tex+'"/>');
				$(this).html(textarea);
				textarea.focus();
				textarea.blur(function(){
					var currentText = textarea.val();
					if(currentText != "")
						if(currentText == tex)
							textarea.parent().text(tex);
						else{
							var pid = textarea.parent().parent().find("#page").val();
							var cid = textarea.parent().parent().find("#cat").val();
							$.post("upcat.php?cat",
							{
								page:pid,
								cat:cid,
								nazwa:currentText
							});
							textarea.parent().text(currentText);
						}
					else 
						textarea.parent().text(tex);
				});
		  }});
		  $("#editor #gird_edit_pag").bind({
			click:function(){
				var tex = $(this).text();
				var textarea = $('<input type="text"style="width:100%; height: 20px;" id="gird_edit_pag" value="'+tex+'"/>');
				$(this).html(textarea);
				textarea.focus();
				textarea.blur(function(){
					var currentText = textarea.val();
					if(currentText != "")
						if(currentText == tex)
							textarea.parent().text(tex);
						else{
							var pid = textarea.parent().parent().find("#page").val();
							$.post("upcat.php?page",
							{
								page:pid,
								nazwa:currentText
							});
							textarea.parent().text(currentText);
						}
					else 
						textarea.parent().text(tex);
				});
		  }}); 
		});
		</script>
	</head>
	<body>
    <table class="all">
    	<tr>
        	<td class="menu">
            	<a href="admin.php?page=cat">Kategorie</a><br />
                <a href="admin.php?page=art">Artykuły</a><br />
                <a href="index.php">Strona Główna</a><br />
            </td>
            <td class="main">
<?
			if(isset($_GET["page"]) && $_GET["page"] == "cat"){
				if(isset($_GET["add"])){
					$sql = "INSERT INTO Categories(PageID, CategoryName) VALUES (".$_POST["page"].", '".$_POST["cat"]."')";
					mysql_query($sql) or die(mysql_errno().":".mysql_error());
				}
				$sql = "SELECT Categories.*, Pages.PageName FROM Categories INNER JOIN Pages ON Categories.PageID=Pages.PageID";
				$q = mysql_query($sql) or die(mysql_errno().":".mysql_error());
				echo '<center><table id="editor" class="kat"><tr class="kat_nag"><td></td><td class="kat_td">Nazwa Strony</td><td class="kat_td">Nazwa Kategorii</td></tr>';
				while($r = mysql_fetch_array($q)){
					echo '<tr class="kat_row"><td><input type="hidden" value="'.$r["PageID"].'" id="page"/><input type="hidden" value="'.$r["CategoryID"].'" id="cat"/></td><td id="gird_edit_pag" class="kat_td">'.$r["PageName"].'</td><td id="gird_edit_kat" class="kat_td">'.$r["CategoryName"].'</td></tr>';
				}
				echo '</table></center>';
				$sql2 = "SELECT * FROM Pages";
				$q2 = mysql_query($sql2) or die(mysql_errno().":".mysql_error());
				$pages = '';
				while($r2 = mysql_fetch_array($q2)){
					$pages = $pages . '<option value="'.$r2["PageID"].'">'.$r2["PageName"].'</option>';
				}
				?>
                	<form method="post" action="admin.php?page=cat&amp;add">
                    <select name="page"><?php echo $pages; ?></select>
                    <input type="text" name="cat"/>
                    <input type="submit" value="Dodaj"/>
                    </form>
                <?php
			}
			else if(isset($_GET["page"]) && $_GET["page"] == "art"){
				$sql = "SELECT * FROM Article";
				$q = mysql_query($sql) or die(mysql_errno().":".mysql_error());
				echo '<center><table class="art"><tr class="art_nag"><td class="kat_td">Strona</td><td class="kat_td">Kategoria</td><td class="art_td">Tytuł Artykułu</td><td class="art_td"></td></tr>';
				while($r = mysql_fetch_array($q)){
					$s = "SELECT PageName FROM Pages WHERE PageID=(SELECT PageID FROM Articles WHERE ArticleID=".$r["ArticleID"].")";
					$s2 = "SELECT CategoryName FROM Categories WHERE PageID=(SELECT PageID FROM Articles WHERE ArticleID=".$r["ArticleID"].") AND CategoryID=(SELECT CategoryID FROM Articles WHERE ArticleID=".$r["ArticleID"].")";
					$r1 = mysql_fetch_array(mysql_query($s));
					$r2 = mysql_fetch_array(mysql_query($s2));
					echo '<tr class="art_row"><td class="kat_td">'.$r1["PageName"].'</td><td class="kat_td">'.$r2["CategoryName"].'</td><td class="art_td">'.$r["Title"].'</td><td class="art_td"><a href="admin.php?page=art&amp;art='.$r["ArticleID"].'">Edytuj</a></td></tr>';
				}
				echo '</table>';
				if( isset($_GET["art"])) $act = "save";
				else $act = "add";
				if( isset($_GET["art"])) $btn = "Zapisz";
				else $btn = "Dodaj";
				$t="";
				$ft="";
				$st="";
				$tt="";
				$fm="";
				$sm="";
				$tm="";
				if(isset($_GET["art"]) && !isset($_GET["save"])){
					
				}
				?>
                	<form method="post" action="admin.php?page=art&amp;<?php echo $act; ?>">
                    Tytuł: <input type="text" name="title"style="width:400px; height:20px;"/><br>
                    Pierwszy Podtytuł: <input type="text" name="ftitle"style="width:400px; height:20px;"/><br>
                    <textarea name="ftext" style="width:500px; height:200px;"></textarea><br>
                    Drugi Podtytuł: <input type="text" name="stitle"style="width:400px; height:20px;"/><br>
                    <textarea name="stext"  style="width:500px; height:200px;"></textarea><br>
                    Trzeci Podtytuł: <input type="text" name="ttitle"style="width:400px; height:20px;"/><br>
                    <textarea name="fttext" style="width:500px; height:200px;"></textarea><br>
                    <input type="submit" value="<?php echo $btn; ?>"/>
                    </form>
                <?php
			}
?>
            </td>
        </tr>
    </table>
	</body>
</html>