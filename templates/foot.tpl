				<div id="FOOT">   
					<br />
					<div class="footer"><span class="footers-font">&copy; 2013 Compendium of WARHAMMER. Wszelkie prawa zastrzeżone</span></div><br />
					<div class="footer"><span class="footers-font">Layout wykonany przez sEbeQ13.</span></div>
					<div class="footer"><span class="footers-font">Strona wykonana i opracowana przez: Michał "Manveru" Świątek</span></div>
					<div class="footer"><span class="footers-font"><div id="data"></div></span></div>
					<div class="footer"><span class="footers-font"><div id="zegarek"></div></span></div>
					<script>
						DayName = new Array(7) 
						DayName[0] = "Niedziela" 
						DayName[1] = "Poniedziałek" 
						DayName[2] = "Wtorek" 
						DayName[3] = "Środa" 
						DayName[4] = "Czwartek" 
						DayName[5] = "Piątek" 
						DayName[6] = "Sobota"
						MonthName = new Array(12) 
						MonthName[0] = "Stycznia" 
						MonthName[1] = "Lutego" 
						MonthName[2] = "Marca" 
						MonthName[3] = "Kwietnia" 
						MonthName[4] = "Maja" 
						MonthName[5] = "Czerwca" 
						MonthName[6] = "Lipca" 
						MonthName[7] = "Sierpnia" 
						MonthName[8] = "Września" 
						MonthName[9] = "Października" 
						MonthName[10] = "Listopada" 
						MonthName[11] = "Grudnia"
						function zegar() {
							var data = new Date();
							var godzina = data.getHours();
							var min = data.getMinutes();
							var sek = data.getSeconds();
							var WeekDay = data.getDay(); 
							var Month = data.getMonth(); 
							var Day = data.getDate(); 
							var Year = data.getFullYear();
							if(Year <= 99) Year += 1900;
							var terazmamy = "Dzisiaj jest "+DayName[WeekDay]+" "+Day+" "+ MonthName[Month]+" roku pańskiego "+Year;
							var terazjest = "Godzina: "+((godzina<10)?"0":"")+godzina+((min<10)?":0":":")+min+((sek<10)?":0":":")+sek;
							document.getElementById("zegarek").innerHTML = terazjest;
							document.getElementById("data").innerHTML = terazmamy;
							setTimeout("zegar()", 1000); 
						}
						zegar();
					</script>
				</div>
			</div>
		</center>
    </body>
</html>
