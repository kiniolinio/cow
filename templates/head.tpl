<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="Styles/containers.css" rel="stylesheet" type="text/css" />
		<link href="Styles/layout.css" rel="stylesheet" type="text/css" />
        <link href="Styles/design.css" rel="stylesheet" type="text/css" />
        <title>{$site}</title>
    </head>
    <body>
		<center>
			<div id="container">
				<div id="HEAD">
					<img src="../Images/logo.png" alt="COW" />
				</div>
				<div id="MENU-LEFT">
					<div class="ul"></div><div class="ur"></div><div class="uc"><br style="font-size: 13px;" /><span class="headers-font">{$page[0]}</span></div>
                    <div class="cc">
						<div class="menu_item"><a class="menu-font" href="index.php">{$cat[0][0][1]}</a></div>
						<div class="menu_item"><a class="menu-font" href="">{$cat[0][1][1]}</a></div>
						<div class="menu_item"><a class="menu-font" href="">{$cat[0][2][1]}</a></div>
						<div class="menu_item"><a class="menu-font" href="index.php?page=contact">{$cat[0][3][1]}</a></div>
					</div>
					<div class="dl"></div><div class="dr"></div><div class="dc"></div>
					<div class="ul"></div><div class="ur"></div><div class="uc"><br style="font-size: 13px;" /><span class="headers-font">{$page[1]}</span></div>
                    <div class="cc">
						{section name=cat2 loop=$cat[1]}
							<div class="menu_item"><a class="menu-font" href="articles.php?cat={$cat[1][cat2][0]}">{$cat[1][cat2][1]}</a></div>
						{/section}
					</div><div class="dl"></div><div class="dr"></div><div class="dc"></div>
				</div>
				<div id="MENU-RIGHT">
					<div class="ul"></div><div class="ur"></div><div class="uc"><br style="font-size: 13px;" /><span class="headers-font">{$page[2]}</span></div>
                    <div class="cc">
						{section name=cat3 loop=$cat[2]}
							<div class="menu_item"><a class="menu-font" href="articles.php?cat={$cat[2][cat3][0]}">{$cat[2][cat3][1]}</a></div>
						{/section}
					</div><div class="dl"></div><div class="dr"></div><div class="dc"></div>
					<div class="ul"></div><div class="ur"></div><div class="uc"><br style="font-size: 13px;" /><span class="headers-font">{$page[3]}</span></div>
                    <div class="cc">
						{section name=cat4 loop=$cat[3]}
							<div class="menu_item"><a class="menu-font" href="articles.php?cat={$cat[3][cat4][0]}">{$cat[3][cat4][1]}</a></div>
						{/section}
					</div><div class="dl"></div><div class="dr"></div><div class="dc"></div>
				</div>